const path = require("node:path");
require("@html-validate/eslint-config/patch/modern-module-resolution");

module.exports = {
	root: true,
	extends: ["@html-validate"],

	overrides: [
		{
			/* ensure cjs and mjs files are linted too */
			files: ["*.cjs", "*.mjs"],
		},
		{
			files: "*.ts",
			extends: ["@html-validate/typescript"],
		},
		{
			files: ["server/**/*.ts"],
			excludedFiles: ["server/**/*.spec.ts"],
			parserOptions: {
				tsconfigRootDir: path.join(__dirname, "server"),
				project: ["./tsconfig.json"],
			},
			extends: ["@html-validate/typescript-typeinfo"],
		},
		{
			files: ["client/**/*.ts"],
			excludedFiles: ["client/**/*.spec.ts"],
			parserOptions: {
				tsconfigRootDir: path.join(__dirname, "client"),
				project: ["./tsconfig.json"],
			},
			extends: ["@html-validate/typescript-typeinfo"],
		},
		{
			/* files which should lint even if project isn't build yet */
			files: ["./*.d.ts", "bin/*.js"],
			rules: {
				"import/export": "off",
				"import/extensions": "off",
				"import/no-unresolved": "off",
			},
		},
	],
};

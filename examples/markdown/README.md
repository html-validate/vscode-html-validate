# Markdown example

This example features HTML in Markdown.

## Vanilla HTML

```html
<h1></h1>
```

## Vue.js

```vue
<template>
  <button :type="type">
    <a href="#">Lorem ipsum</a>
  </button>
</template>
```

import {
	type CodeActionParams,
	type Diagnostic,
	CodeAction,
	Command,
	CodeActionKind,
} from "vscode-languageserver";
import { URI } from "vscode-uri";
import { type HtmlValidate } from "html-validate";
import { type ApiContext } from "./api-context";

const enum COMMAND {
	RULE_DOCUMENTATION = "html-validate.rule-documentation",
}

/**
 * Create code action to open rule documentation if a URL is available.
 */
async function ruleDocumentationAction(
	htmlvalidate: HtmlValidate,
	diagnostic: Diagnostic,
): Promise<CodeAction | null> {
	/* only handle diagnostic messages for this extension */
	if (diagnostic.source !== "html-validate" || !diagnostic.code) {
		return null;
	}

	/* fetch rule documentation */
	const ruleId = diagnostic.code as string;
	/* eslint-disable-next-line @typescript-eslint/no-deprecated -- needed for backwards compatibility */
	const documentation = await htmlvalidate.getRuleDocumentation(ruleId);
	if (!documentation) {
		return null;
	}

	const title = `Show documentation for ${ruleId}`;
	const command = Command.create(title, COMMAND.RULE_DOCUMENTATION, documentation.url);

	return CodeAction.create(title, command, CodeActionKind.QuickFix);
}

/**
 * Create code actions for diagnostic messages.
 */
export async function onCodeAction(
	this: ApiContext,
	params: CodeActionParams,
): Promise<CodeAction[]> {
	const { context } = params;
	const resource = params.textDocument.uri;
	const uri = URI.parse(resource);

	const htmlvalidate = await this.getDocumentValidator(uri.path);
	if (!htmlvalidate) {
		return [];
	}

	const actions: CodeAction[] = [];

	for (const diagnostic of context.diagnostics) {
		try {
			const action = await ruleDocumentationAction(htmlvalidate, diagnostic);
			if (action) {
				actions.push(action);
			}
		} catch (error) {
			this.trace(String(error));
		}
	}

	return actions;
}

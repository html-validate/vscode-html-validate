import { type Diagnostic } from "vscode-languageserver";
import { type RuleDocumentation } from "html-validate";

export interface ExtendedDiagnostic extends Diagnostic {
	documentation: RuleDocumentation | null;
}

import { type HoverParams, type Hover, type Range, type Position } from "vscode-languageserver";
import { type ApiContext } from "./api-context";

function containsPosition(range: Range, position: Position): boolean {
	if (position.line < range.start.line || position.line > range.end.line) {
		return false;
	}
	if (position.line === range.start.line && position.character < range.start.character) {
		return false;
	}
	if (position.line === range.end.line && position.character > range.end.character) {
		return false;
	}
	return true;
}

/**
 * Show extended documentation for a diagnostic message.
 */
export async function onHover(this: ApiContext, params: HoverParams): Promise<Hover | null> {
	const resource = params.textDocument.uri;
	const diagnostics = this.getDocumentDiagnostics(resource);
	if (diagnostics.length === 0) {
		return null;
	}

	const diagnostic = diagnostics.find((cur) => {
		return containsPosition(cur.range, params.position);
	});
	if (!diagnostic) {
		return null;
	}

	const htmlvalidate = await this.getDocumentValidator(resource);
	if (!htmlvalidate) {
		return null;
	}

	const ruleId = diagnostic.code as string;
	const documentation = diagnostic.documentation;
	if (!documentation) {
		const contents = [`HTML-Validate`, diagnostic.message].join("\n\n");
		return { contents };
	}

	const contents = [
		`HTML-Validate`,
		documentation.description,
		documentation.url ? `[${ruleId}](${documentation.url})` : ruleId,
	].join("\n\n");

	return { contents };
}

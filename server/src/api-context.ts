import { type HtmlValidate } from "html-validate";
import { type ExtendedDiagnostic } from "./extended-diagnostic";

export interface ApiContext {
	getDocumentDiagnostics(resource: string): ExtendedDiagnostic[];
	getDocumentValidator(resource: string): Promise<HtmlValidate | null>;
	trace(message: string, verbose?: string): void;
}

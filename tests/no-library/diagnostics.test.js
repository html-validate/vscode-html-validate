const path = require("path");
const vscode = require("vscode");

const chai = import("chai");

async function activate(docUri) {
	const ext = vscode.extensions.getExtension("html-validate.vscode-html-validate");
	await ext.activate();
	try {
		const doc = await vscode.workspace.openTextDocument(docUri);
		await vscode.window.showTextDocument(doc);
		await sleep(10000); // Wait for server activation
	} catch (e) {
		/* eslint-disable-next-line no-console -- for debugging */
		console.error(e);
	}
}

async function sleep(ms) {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

function getDocUri(filename) {
	const filePath = path.resolve(__dirname, filename);
	return vscode.Uri.file(filePath);
}

function toRange(sLine, sChar, eLine, eChar) {
	const start = new vscode.Position(sLine, sChar);
	const end = new vscode.Position(eLine, eChar);
	return new vscode.Range(start, end);
}

describe("vscode-html-validate", () => {
	it("should not find errors in test.html", async () => {
		const { expect } = await chai;
		const docUri = getDocUri("test.html");
		await activate(docUri);
		const actualDiagnostics = vscode.languages.getDiagnostics(docUri);
		expect(actualDiagnostics).to.eql([
			{
				data: undefined,
				hasDiagnosticCode: false,
				source: "html-validate",
				message: '<button> is missing recommended "type" attribute',
				range: toRange(0, 1, 0, 7),
				severity: vscode.DiagnosticSeverity.Error,
				code: "no-implicit-button-type",
			},
		]);
	});

	it("should not find errors in test.txt", async () => {
		const { expect } = await chai;
		const docUri = getDocUri("test.txt");
		await activate(docUri);
		const actualDiagnostics = vscode.languages.getDiagnostics(docUri);
		expect(actualDiagnostics).to.eql([]);
	});
});

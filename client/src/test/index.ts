/* eslint-disable no-console -- expected to log */

import path from "path";
import Mocha from "mocha";
import { glob } from "glob";

export async function run(): Promise<void> {
	const mocha = new Mocha({
		ui: "bdd",
		color: true,
	});
	mocha.timeout(30000);

	const testsRoot = process.env.TESTS_ROOT ?? path.resolve(__dirname, "..");

	const files = await glob("**/**.test.js", { cwd: testsRoot, ignore: "node_modules/**" });
	console.group("Test files:");
	for (const filename of files) {
		console.log(filename);
		mocha.addFile(path.resolve(testsRoot, filename));
	}
	console.groupEnd();

	return new Promise((resolve, reject) => {
		try {
			mocha.run((failures) => {
				if (failures > 0) {
					reject(new Error(`${String(failures)} tests failed.`));
				} else {
					resolve();
				}
			});
		} catch (err: unknown) {
			reject(err instanceof Error ? err : new Error(String(err)));
		}
	});
}

/* eslint-disable-next-line import/no-unresolved -- will be resolved when running inside vscode */
import vscode from "vscode";
import { getDocUri, activate } from "./helper";

declare module "vscode" {
	interface Diagnostic {
		data?: unknown;
		hasDiagnosticCode: boolean;
	}
}

const chai = import("chai");

describe("vscode-html-validate", () => {
	it("should find errors in html/index.html", async () => {
		const { expect } = await chai;
		const docUri = getDocUri("html/index.html");
		await activate(docUri);
		const actualDiagnostics = normalize(vscode.languages.getDiagnostics(docUri));
		expect(actualDiagnostics).to.eql([
			makeError({
				message: '<button> is missing recommended "type" attribute',
				range: toRange(0, 1, 0, 7),
				severity: vscode.DiagnosticSeverity.Warning,
				code: "no-implicit-button-type",
			}),
		]);
	});

	it("should not find errors in text/plaintext.txt", async () => {
		const { expect } = await chai;
		const docUri = getDocUri("text/plaintext.txt");
		const actualDiagnostics = normalize(vscode.languages.getDiagnostics(docUri));
		await activate(docUri);
		expect(actualDiagnostics).to.eql([]);
	});

	describe("angular", () => {
		it("should find errors in angularjs/my-component.html", async () => {
			const { expect } = await chai;
			const docUri = getDocUri("angularjs/my-component.html");
			await activate(docUri);
			const actualDiagnostics = normalize(vscode.languages.getDiagnostics(docUri));
			expect(actualDiagnostics).to.eql([
				makeError({
					message: "<a> element is not permitted as a descendant of <button>",
					range: toRange(3, 2, 3, 3),
					code: "element-permitted-content",
				}),
			]);
		});

		it("should find errors in angularjs/my-component.js", async () => {
			const { expect } = await chai;
			const docUri = getDocUri("angularjs/my-component.js");
			await activate(docUri);
			const actualDiagnostics = normalize(vscode.languages.getDiagnostics(docUri));
			expect(actualDiagnostics).to.eql([
				makeError({
					message: "<h1> cannot be empty, must have text content",
					range: toRange(2, 13, 2, 15),
					code: "empty-heading",
				}),
			]);
		});
	});

	describe("markdown", () => {
		it("should find errors in markdown/README.md", async () => {
			const { expect } = await chai;
			const docUri = getDocUri("markdown/README.md");
			await activate(docUri);
			const actualDiagnostics = normalize(vscode.languages.getDiagnostics(docUri));
			expect(actualDiagnostics).to.eql([
				makeError({
					message: "<h1> cannot be empty, must have text content",
					range: toRange(7, 1, 7, 3),
					code: "empty-heading",
				}),
				makeError({
					message: "<a> element is not permitted as a descendant of <button>",
					range: toRange(15, 5, 15, 6),
					code: "element-permitted-content",
				}),
			]);
		});
	});

	describe("vue", () => {
		it("should find errors in vue/main.js", async () => {
			const { expect } = await chai;
			const docUri = getDocUri("vue/main.js");
			await activate(docUri);
			const actualDiagnostics = normalize(vscode.languages.getDiagnostics(docUri));
			expect(actualDiagnostics).to.eql([
				makeError({
					message: '<button> is missing recommended "type" attribute',
					range: toRange(5, 13, 5, 19),
					code: "no-implicit-button-type",
				}),
			]);
		});
	});
});

function toRange(sLine: number, sChar: number, eLine: number, eChar: number): vscode.Range {
	const start = new vscode.Position(sLine, sChar);
	const end = new vscode.Position(eLine, eChar);
	return new vscode.Range(start, end);
}

function makeError(partial: Partial<vscode.Diagnostic>): vscode.Diagnostic {
	return {
		message: "",
		range: toRange(0, 0, 0, 0),
		severity: vscode.DiagnosticSeverity.Error,
		code: "no-implicit-close",
		data: undefined,
		hasDiagnosticCode: false,
		source: "html-validate",
		...partial,
	};
}

const reOldElementPermittedContent =
	/^Element <([^>]+)> is not permitted as descendant of <([^>]+)>$/;

function normalize(diagnostics: vscode.Diagnostic[]): vscode.Diagnostic[] {
	return diagnostics.map((it) => {
		const match = reOldElementPermittedContent.exec(it.message);
		if (match) {
			it.message = `<${match[1]}> element is not permitted as a descendant of <${match[2]}>`;
		}
		return it;
	});
}

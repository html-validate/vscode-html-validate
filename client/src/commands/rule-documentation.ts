import vscode, {
	Uri,
} from "vscode"; /* eslint-disable-line import/no-unresolved -- will be resolved when running inside vscode */

export async function ruleDocumentation(url: string): Promise<void> {
	if (!url) {
		return;
	}
	await vscode.env.openExternal(Uri.parse(url));
}
